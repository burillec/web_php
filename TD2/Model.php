<?php
require_once 'Conf.php';
class Model {
    static private $pdo = NULL;

    static public function init() {
        $hostname = Conf::getHostname();
        $database_name=Conf::getDatabase();
        $login=Conf::getLogin();
        $password=Conf::getPassword();
        $port = 3306;


        try{
            self::$pdo = new PDO("mysql:host=$hostname;port=$port;dbname=$database_name",$login,$password,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

            // On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
            self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            if (Conf::getDebug()) {
                echo $e->getMessage(); // affiche un message d'erreur
            } else {
                echo 'Une erreur est survenue <a href=""> retour a la page d\'accueil </a>';
            }
            die();
        }


        return self::$pdo;
    }

    static public function getPDO() {
    if(is_null(self::$pdo)){
        self::init();
    }
        return self::$pdo;
    }

}
?>

