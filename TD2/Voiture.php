<?php
class Voiture {

    private $marque;
    private $couleur;
    private $immatriculation;

    public function getMarque(){
        return $this->marque;
    }

    public function setMarque($m){
        $this->marque = $m;
    }

    public function getImmatricualtion(){
        return $this->immatricualtion;
    }
    public function setImmatricualtion($i){
        if(strlen($i) <=8){
            $this->immatricualtion = $i;
        }
    }

    public function getCouleur(){
        return $this->couleur;
    }
    public function setCouleur($c){
        $this->couleur = $c;
    }

    public function __construct($m = NULL, $c = NULL, $i = NULL)
    {
        if (!is_null($m) && !is_null($c) && !is_null($i)) {
            $this->marque = $m;
            $this->couleur = $c;
            $this->immatricualtion = $i;
        }
    }

    public function afficher(){
        echo "<p> Voiture {$this->immatriculation} de marque {$this->marque} (Couleur {$this->couleur}) <p>";
    }


    public static function getAllVoitures(){
        $tab_obj = new ArrayObject();

        $pdo =  Model::getPDO();
        $rep = $pdo-> query("SELECT * FROM voiture");
        $rep->setFetchMode(PDO::FETCH_CLASS,"Voiture");
        $tab_obj = $rep->fetchAll();



        return $tab_obj;
    }
}

?>