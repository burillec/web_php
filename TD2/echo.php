<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>

    <body>
    <?php
    $marque= "Renault";
    $immatriculation = "256AB34";
    $couleur ="bleu";

    echo "<p> Voiture $immatriculation de marque $marque (Couleur $couleur) </p> ";

    $voiture["marque"] = "Renault";
    $voiture["immatriculation"] = "256AB34";
    $voiture["couleur"] = "bleu";

    echo "<p> Voiture {$voiture["immatriculation"]} de marque {$voiture["marque"]} (Couleur {$voiture["couleur"]}) </p> ";


    $voitures = array();
    $voitures[] = $voiture;
    $voitures[] = array(
      "marque" => "Renault",
      "couleur"    => "bleu",
      "immatriculation" => "256AB34");



    if (empty($voitures)){
        echo "<h3>Il n'y a aucune voiture.</h3>";
    }else{
        echo "<h3>Liste des voitures :</h3>";
        echo "<ul>";
        foreach($voitures as $v){
            echo "<li> Voiture {$v["immatriculation"]} de marque {$v["marque"]} (Couleur {$v["couleur"]}) </li>";
        }
        echo "</ul>";
        }



?>




    </body>
</html>