<!<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Document</title>
</head>
<body>

    <?php
// On inclut les fichiers de classe PHP avec require_once
// pour éviter qu'ils soient inclus plusieurs fois
require_once 'Conf.php';
require_once 'Model.php';

// On affiche le login de la base de donnees
echo Conf::getLogin() . "<br>";
echo Conf::getHostname(). "<br>";
echo Conf::getDatabase(). "<br>";
echo Conf::getPassword(). "<br>";

//echo Model::getPDO(). "<br>";
?>
</body>
</html>
